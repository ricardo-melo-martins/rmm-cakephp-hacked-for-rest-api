FROM php:7.4-apache

RUN apt-get update

## Configure Apache
RUN a2enmod rewrite \
    && sed -i 's!/var/www/html!/var/www/webroot!g' /etc/apache2/sites-available/000-default.conf \
    && mv /var/www/html /var/www/webroot

RUN echo "ServerName localhost" >> /etc/apache2/apache2.conf 
# &&\
#     a2enmod rewrite &&\
#     a2dissite 000-default &&\
#     a2ensite my-apache-site &&\
#     service apache2 restart

## Install Composer
RUN curl -sS https://getcomposer.org/installer \
  | php -- --install-dir=/usr/local/bin --filename=composer


RUN apt-get install --yes rsync

###
## PHP Extensisons
###

## Install zip libraries and extension
RUN apt-get install --yes git zlib1g-dev libzip-dev \
    && docker-php-ext-install zip

## Install intl library and extension
RUN apt-get install --yes libicu-dev \
    && docker-php-ext-configure intl \
    && docker-php-ext-install intl

###
## Optional PHP extensions 
###

## mbstring for i18n string support
# RUN docker-php-ext-install mbstring


## MySQL PDO support
# RUN docker-php-ext-install pdo_mysql

## PostgreSQL PDO support
# RUN apt-get install --yes libpq-dev \
#     && docker-php-ext-install pdo_pgsql

## APCU
# RUN pecl install apcu \
#     && docker-php-ext-enable apcu

## Memcached
# RUN apt-get install --yes libmemcached-dev \
#     && pecl install memcached \
#     && docker-php-ext-enable memcached

## MongoDB
# RUN pecl install mongodb \
#     && docker-php-ext-enable mongodb

## Redis support.  igbinary and libzstd-dev are only needed based on 
## redis pecl options
# RUN pecl install igbinary \
#     && docker-php-ext-enable igbinary \
#     && apt-get install --yes libzstd-dev \
#     && pecl install redis \
#     && docker-php-ext-enable redis


WORKDIR /var/www

RUN mkdir -p /folder-shared

COPY . .

RUN groupadd -g 1000 www
RUN useradd -u 1000 -ms /bin/bash -g www www

RUN chown www:www /var/www/

USER www

WORKDIR /var/www

RUN ln -sf /dev/stdout /var/log/apache2/error.log && \
    ln -sf /dev/stderr /var/log/apache2/access.log


# //CMD ["bash", "-c", "sh sync.sh"]