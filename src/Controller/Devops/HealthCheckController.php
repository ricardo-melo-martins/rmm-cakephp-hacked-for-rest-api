<?php
declare(strict_types=1);

namespace App\Controller\Devops;

use App\Controller\AppController;

use Cake\Core\Configure;
use Cake\Http\Exception\ForbiddenException;
use Cake\Http\Exception\NotFoundException;
use App\Handlers\Http\Response;

class HealthCheckController extends AppController
{
    
    public function index( )
    {
        
        $data = [];

        try {

            $this->set(compact('data'));


        } catch (ForbiddenException $exception) {
            if (Configure::read('debug')) {
                throw $exception;
            }
            throw new NotFoundException();
        }
    }
}
