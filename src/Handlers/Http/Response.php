<?php
declare(strict_types=1);

namespace App\Handlers\Http\Middleware;

use Cake\Http\Response as CakeResponse;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\StreamInterface;

/**
 * Parse encoded request body data.
 *
 * Enables JSON and XML request payloads to be parsed into the request's body.
 * You can also add your own request body parsers using the `addParser()` method.
 */
class Response extends CakeResponse implements ResponseInterface
{

}