# RMM CakePHP Hacked for Rest Api Skeleton

A skeleton for creating applications with [CakePHP](https://cakephp.org) 4.x.

You can now either use your machine's webserver to view the default home page, or start
up the built-in webserver with:

```bash
bin/cake server -p 8765
```

Then visit `http://localhost:8765` to see the welcome page.

## Configuration

Read and edit the environment specific `config/app_local.php` and setup the 
`'Datasources'` and any other configuration relevant for your application.
Other environment agnostic settings can be changed in `config/app.php`.

## Docker

```bash
$ docker-compose up -d --build
```

Sync code

```bash
$ docker exec -it 3657 bash
```
